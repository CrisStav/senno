# SENNO TEST

## Visão Geral
Aplicacao criada a partir do desafio proposto.

## Tecnologias e Bibliotecas
 - NodeJs;
 - ExpressJs;
 - Mongoose;
 - Babel;
 - Cors;
 - Faker;
 - Json Wen Token;

## Iniciando a Aplicação
para iniciar a aplicação, execute os comando as seguir.

 ```
    $ npm instal  
 ```
 ```
    $ npm start  
 ``` 

## Rotas
Rotas da aplicação.

#### Todas as chamadas da aplicação (exceto o Login) devem conter o token gerado no Login no header da requisição. 
#### Ex: Authorization: {token Gerado no Login};

 Login - `POST` (http://localhost:3000/login)
  - body: { email, password }

 Lista as workspaces do usuario - `GET` (http://localhost:3000/my_workspaces);

 TOKEN JWT com a informação da WORKSPACE selecionada - `GET` (http://localhost:3000/set_workspaces/{ID_DA_WORKSPACE});

 Lista todas as ideias na WORKSPACE - `GET` (http://localhost:3000/ideas);

 Lista todas as ideias do usuario na workspace - `GET` (http://localhost:3000/my_ideas);
