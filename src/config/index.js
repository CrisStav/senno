import 'dotenv/config';
import express from 'express';
import cors from 'cors';
import connectToDb from '../database';
import { userRoutes, ideasRoutes, workspaceRoutes } from '../routes';

const app = express();

app.use(cors({ origin: '*' }));

app.use(express.json());

app.use(userRoutes);
app.use(ideasRoutes);
app.use(workspaceRoutes);

connectToDb();

export default app;
