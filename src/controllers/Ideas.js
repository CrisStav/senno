/* eslint-disable class-methods-use-this */
import IdeasRepositoy from '../repository/Ideas';
import WorkspaceRepositoy from '../repository/Workspace';

const ideasRepositoy = new IdeasRepositoy();
const workspaceRepositoy = new WorkspaceRepositoy();

export default class IdeasController {
  async getMyIdeas(req, res) {
    const { token } = req;
    const { _id } = token;

    try {
      const ideas = await ideasRepositoy.getUser(_id);

      const ideaIds = ideas.map((idea) => idea._id);

      const workspace = await workspaceRepositoy.getUserIdeas(ideaIds);

      return res.status(200).json(workspace);
    } catch (err) {
      return res.status(500).json(err);
    }
  }

  async getIdeas(req, res) {
    const { token } = req;
    const { _id } = token;

    const ideas = await ideasRepositoy.getUser(_id);

    const ideaIds = ideas.map((idea) => idea._id);

    const workspace = await workspaceRepositoy.getWorkspaceIdeas(ideaIds);

    return res.status(200).json(workspace);
  }
}
