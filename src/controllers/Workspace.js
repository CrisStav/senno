/* eslint-disable class-methods-use-this */
import 'dotenv/config';
import jwt from 'jsonwebtoken';
import WorkspaceRepository from '../repository/Workspace';

const workspaceRepository = new WorkspaceRepository();

export default class Workspace {
  async getMyWorkspaces(req, res) {
    const { _id } = req.token;
    try {
      const workspaces = await workspaceRepository.getUserWorkspaces(_id);

      return res.json(workspaces);
    } catch (err) {
      return res.status(500).json(err);
    }
  }

  async setWorkspace(req, res) {
    const { token } = req;
    const { workspace } = req.params;

    try {
      const dbWorkspace = await workspaceRepository.getWorkspace(workspace);

      const newToken = jwt.sign({ token, dbWorkspace }, process.env.SECRET);

      return res.json({ token: newToken });
    } catch (error) {
      return res.status(500).json(error);
    }
  }
}
