/* eslint-disable class-methods-use-this */
import jwt from 'jsonwebtoken';
import UserRepository from '../repository/User';

const userRepository = new UserRepository();

export default class UserController {
  async Login(req, res) {
    const { email, password } = req.body;

    try {
      const dbUser = await userRepository.Login(email, password);

      if (!dbUser) {
        return res.status(404).json({ error: 'user not found' });
      }

      const token = jwt.sign(dbUser, process.env.SECRET, { expiresIn: '8h' });

      return res.json({ token });
    } catch (error) {
      return res.status(500).send(error);
    }
  }
}
