/* eslint-disable no-param-reassign */
import { createServer, proxy } from 'aws-serverless-express';
import app from './config';

const server = createServer(app);
exports.handler = (event, context) => {
  console.log('EVENT ===>', event);
  console.log('CONTEXT ===>', context);
  context.callbackWaitsForEmptyEventLoop = false;

  return proxy(server, event, context);
};
