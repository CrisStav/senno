/* eslint-disable class-methods-use-this */
import UserModel from '../models/user';

export default class UserRepository {
  async Login(email, password) {
    return UserModel.findOne({ email, password }).lean();
  }
}
