/* eslint-disable class-methods-use-this */
import { Types } from 'mongoose';
import WorkspaceModel from '../models/workspaces';

export default class Workspacerepository {
  async getUserIdeas(ideaIds) {
    return WorkspaceModel
      .aggregate()
      .lookup({
        from: 'ideas',
        localField: 'ideas.id',
        foreignField: '_id',
        as: 'ideas',
      })
      .match({
        'ideas._id': { $in: ideaIds },
      })
      .project({
        ideas: {
          $filter: {
            input: '$ideas',
            as: 'ideas',
            cond: { $in: ['$$ideas._id', ideaIds] },
          },
        },
        name: 1,
      });
  }

  async getWorkspaceIdeas(ideaIds) {
    return WorkspaceModel.aggregate()
      .lookup({
        from: 'ideas',
        localField: 'ideas.id',
        foreignField: '_id',
        as: 'ideas',
      })
      .match({
        'ideas._id': { $in: ideaIds },
      });
  }

  async getUserWorkspaces(_id) {
    return WorkspaceModel
      .aggregate()
      .lookup({
        from: 'users',
        localField: 'users._id',
        foreignField: '_id',
        as: 'users',
      })
      .match({
        'users._id': Types.ObjectId(_id),
      });
  }

  async getWorkspace(workspace) {
    return WorkspaceModel
      .aggregate()
      .lookup({
        from: 'users',
        localField: 'users._id',
        foreignField: '_id',
        as: 'users',
      })
      .lookup({
        from: 'ideas',
        localField: 'ideas.id',
        foreignField: '_id',
        as: 'ideas',
      })
      .match({ _id: Types.ObjectId(workspace) });
  }
}
