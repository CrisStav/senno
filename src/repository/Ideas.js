/* eslint-disable class-methods-use-this */
import IdeasModel from '../models/idea';

export default class IdeasRepository {
  async getUser(_id) {
    return IdeasModel.find({ author: _id }).lean();
  }
}
