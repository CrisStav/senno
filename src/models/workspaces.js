import { Schema, model } from 'mongoose';

const workspaceSchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  ideas: {
    type: Array,
    required: true,
  },
  users: {
    type: Array,
    required: true,
  },
});

const Workspace = model('Workspace', workspaceSchema);

export default Workspace;
