import { Schema, model, Types } from 'mongoose';

const IdeasSchema = new Schema({
  author: {
    type: Types.ObjectId,
    required: true,
  },
  name: {
    type: String,
    required: true,
  },
  description: {
    type: String,
    required: true,
  },
});

const Ideas = model('Ideas', IdeasSchema);

export default Ideas;
