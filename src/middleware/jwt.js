import 'dotenv/config';
import jwt from 'jsonwebtoken';

export default function verifyToken(req, res, next) {
  const token = req.headers.authorization;

  try {
    jwt.verify(token, process.env.SECRET);

    const decoded = jwt.decode(token);

    req.token = decoded;

    return next();
  } catch (error) {
    return res.status(401).json({ error });
  }
}
