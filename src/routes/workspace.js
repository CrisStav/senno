import { Router } from 'express';
import WorkspaceController from '../controllers/Workspace';
import verifyToken from '../middleware/jwt';

const workspaceController = new WorkspaceController();
const routes = Router();

routes.get('/my_workspaces', verifyToken, workspaceController.getMyWorkspaces);
routes.get('/set_workspaces/:workspace', verifyToken, workspaceController.setWorkspace);

export default routes;
