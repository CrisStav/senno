import 'dotenv/config';
import { Router } from 'express';
import UserController from '../controllers/User';
import UserModel from '../models/user';

const userController = new UserController(UserModel);

const routes = Router();

routes.post('/login', userController.Login);

export default routes;
