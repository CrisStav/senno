import { Router } from 'express';
import IdeasController from '../controllers/Ideas';
import verifyToken from '../middleware/jwt';

const routes = Router();
const ideasController = new IdeasController('faker');

routes.get('/my_ideas', verifyToken, ideasController.getMyIdeas);
routes.get('/ideas', verifyToken, ideasController.getIdeas);

export default routes;
