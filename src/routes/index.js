import userRoutes from './user';
import ideasRoutes from './ideas';
import workspaceRoutes from './workspace';

export { userRoutes, ideasRoutes, workspaceRoutes };
