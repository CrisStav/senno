import 'dotenv/config';
import App from './config';

App.listen(process.env.PORT, () => {
  console.log(`Server listening on port ${process.env.PORT}`);
});
