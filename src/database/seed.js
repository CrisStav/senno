import faker from 'faker';
import connectToDb from './index';
import UserModel from '../models/user';
import WorkspaceModel from '../models/workspaces';
import IdeaModel from '../models/idea';


connectToDb()
  .then(async () => {
    try {
      const users = [
        {
          name: faker.name.findName(),
          email: faker.random.word(),
          password: 'senha123',
        },
        {
          name: faker.name.findName(),
          email: faker.random.word(),
          password: 'senha123',
        },
      ];

      console.log('--- inset users START ---');
      await UserModel.insertMany(users);
      const dbUsers = await UserModel.find({}).lean();
      console.log('--- inset users FINISHED ---');


      const ideas = [
        {
          author: dbUsers[0]._id,
          name: faker.random.words(3),
          description: faker.random.words(20),
        },
        {
          author: dbUsers[0]._id,
          name: faker.random.words(3),
          description: faker.random.words(20),
        },
        {
          author: dbUsers[1]._id,
          name: faker.random.words(3),
          description: faker.random.words(20),
        },
        {
          author: dbUsers[1]._id,
          name: faker.random.words(3),
          description: faker.random.words(20),
        },
      ];
      console.log('--- inset Ideas START ---');
      await IdeaModel.create(ideas);
      const persistedIdeas = await IdeaModel.find().lean();
      console.log('--- inset Ideas FINISHED ---');


      const workspaces = [
        {
          name: faker.random.word({ length: 7 }),
          ideas: [
            { _id: persistedIdeas[0]._id },
            { _id: persistedIdeas[1]._id },
            { _id: persistedIdeas[3]._id },
          ],
          users: [
            { _id: dbUsers[0]._id },
            { _id: dbUsers[1]._id },
          ],
        },
        {
          name: faker.random.word({ length: 7 }),
          ideas: [
            { id: persistedIdeas[2]._id },
            { id: persistedIdeas[3]._id },
          ],
        },
      ];
      console.log('--- inset Workspaces START ---');
      await WorkspaceModel.create(workspaces);
      console.log('--- inset Workspaces FINISHED ---');

      return process.exit(0);
    } catch (err) {
      return err;
    }
  });
